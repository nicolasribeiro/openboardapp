import { useEffect, useState } from "react";
import { EntriesContext } from "./";
import { Entry } from "@/interfaces";
import { entriesApi } from "@/httpApis";

interface EntriesProviderProps {
  children: React.ReactNode;
}

export const EntriesProvider = ({
  children,
}: EntriesProviderProps): JSX.Element => {
  const [entries, setEntries] = useState<Entry[]>([]);

  const [isFormOpen, setIsFormOpen] = useState<boolean>(false);

  const updateEntry = async ({
    _id,
    description,
    status,
  }: Entry): Promise<void> => {
    try {
      const { data } = await entriesApi.put<Entry>(`/entries/${_id}`, {
        description,
        status,
      });
      let newEntryArray = entries.map((e) => {
        if (e._id === _id) {
          return data;
        }
        return e;
      });
      setEntries(newEntryArray);
    } catch (error) {
      console.error(error);
    }
  };

  const addNewEntry = async (description: string): Promise<void> => {
    const { data } = await entriesApi.post<Entry>("/entries", { description });

    setEntries([...entries, data]);
  };

  const refreshEntries = async (): Promise<void> => {
    const { data } = await entriesApi.get<Entry[]>("/entries");
    setEntries(data);
  };

  useEffect(() => {
    refreshEntries();
  }, []);

  return (
    <EntriesContext.Provider
      value={{
        entries,
        isFormOpen,
        setEntries,
        setIsFormOpen,
        updateEntry,
        addNewEntry,
      }}
    >
      {children}
    </EntriesContext.Provider>
  );
};
