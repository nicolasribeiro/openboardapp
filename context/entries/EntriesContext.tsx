import { Entry } from "@/interfaces";
import { Context, Dispatch, SetStateAction, createContext } from "react";

interface EntriesContextProps {
  entries: Entry[];
  isFormOpen: boolean;
  setEntries: Dispatch<SetStateAction<Entry[]>>;
  setIsFormOpen: Dispatch<SetStateAction<boolean>>;
  updateEntry: (entry: Entry) => void;
  addNewEntry: (description: string) => void;
}

export const EntriesContext: Context<EntriesContextProps> = createContext(
  {} as EntriesContextProps
);
