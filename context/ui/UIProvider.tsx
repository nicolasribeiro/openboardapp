import { useState } from "react";
import { UIContext } from "./";

interface ProviderProps {
  children?: React.ReactNode | undefined;
}

export const UIProvider = ({ children }: ProviderProps) => {
  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
  const [isDraggin, setIsDraggin] = useState<boolean>(false);

  return (
    <UIContext.Provider
      value={{
        isMenuOpen,
        isDraggin,
        setIsMenuOpen,
        setIsDraggin,
      }}
    >
      {children}
    </UIContext.Provider>
  );
};
