import { Dispatch, SetStateAction, createContext } from "react";

interface ContextProps {
  isMenuOpen: boolean;
  isDraggin: boolean;

  setIsMenuOpen: Dispatch<SetStateAction<boolean>>;
  setIsDraggin: Dispatch<SetStateAction<boolean>>;
}

export const UIContext = createContext({} as ContextProps);
