import { OpenBoardDB } from "@/database";
import { EntryModel, IEntry } from "@/models";
import mongoose from "mongoose";
import { NextApiRequest, NextApiResponse } from "next";

type Data = { msg: string } | IEntry[] | IEntry;
type Query = string | string[] | undefined;

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const { id } = req.query;
  if (!mongoose.isValidObjectId(id)) {
    res.status(400).json({ msg: "Parametro no valido" });
  }

  switch (req.method) {
    case "PUT":
      return updateEntry(req, res, id);
    case "DELETE":
      return deleteEntry(req, res, id);
    case "GET":
      return getEntry(res, id);
    default:
      return res.status(400).json({ msg: "Endpoint invalido" });
  }
}

const updateEntry = async (
  req: NextApiRequest,
  res: NextApiResponse<Data>,
  id: Query
) => {
  await OpenBoardDB.connect();

  const entryToUpdate = await EntryModel.findById(id);
  if (!entryToUpdate) {
    await OpenBoardDB.disconnect();
    res.status(400).json({ msg: `No hay entrada con ID: ${id}` });
    return;
  }

  const {
    description = entryToUpdate.description,
    status = entryToUpdate.status,
  } = req.body;

  try {
    const updatedEntry = await EntryModel.findByIdAndUpdate(
      id,
      { description, status },
      { runValidators: true, new: true }
    );
    await OpenBoardDB.disconnect();
    res.status(200).json(updatedEntry!);
  } catch (error) {
    await OpenBoardDB.disconnect();
    console.error({ error });
    res.status(400).json({ msg: "Error inesperado al actualizar" });
  }

  await OpenBoardDB.disconnect();
};
const deleteEntry = async (
  req: NextApiRequest,
  res: NextApiResponse<Data>,
  id: Query
) => {
  res.status(200).json({ msg: "Funcion delete" });
};
const getEntry = async (res: NextApiResponse<Data>, id: Query) => {
  await OpenBoardDB.connect();
  const entry = await EntryModel.findOne({ _id: id });
  await OpenBoardDB.disconnect();

  if (!entry) {
    await OpenBoardDB.disconnect();
    res.status(400).json({ msg: `No se encontro el ID: ${id}` });
    return;
  }
  res.status(200).json(entry);
};
