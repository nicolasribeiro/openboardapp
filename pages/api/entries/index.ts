import { OpenBoardDB } from '@/database';
import { EntryModel, IEntry } from '@/models';
import { NextApiRequest, NextApiResponse } from 'next';

type Data = 
|{  msg: string; }
| IEntry[]
| IEntry




export default function handler(req:NextApiRequest, res:NextApiResponse<Data>) {


  switch (req.method) {
    case 'GET':
      return getEntries(res)
    case 'POST':
      return setEntries(req, res)
    default:
      return res.status(400).json({ msg: 'Endpoint invalido' })
  }

}


const getEntries = async (res:NextApiResponse<Data>) => {
  await OpenBoardDB.connect()
  const entries = await EntryModel.find().sort({ createdAt: 'ascending' })
  await OpenBoardDB.disconnect()

  res.status(200).json(entries)
}


const setEntries = async (req:NextApiRequest, res:NextApiResponse<Data>) => {
  const { description = '' } = req.body
  const newEntry = new EntryModel({
    description,
    createdAt: Date.now(),
  })

  try {
    OpenBoardDB.connect()
    await newEntry.save()
    OpenBoardDB.disconnect()

    res.status(200).json(newEntry)
  } catch (error) {
    await OpenBoardDB.disconnect()
    console.error(error)
    res.status(500).json({ msg: 'Algo salio mal' })
  }
}