import { OpenBoardDB, seedData } from '@/database';
import { EntryModel } from '@/models';
import { NextApiRequest, NextApiResponse } from 'next';

type Data = {
  msg: string;
}


export default async function handler(req:NextApiRequest, res:NextApiResponse<Data>) {

  if(process.env.NODE_ENV === 'production'){
    res.status(401).json({ msg: 'No tiene acceso a este servicio' })
  }

  await OpenBoardDB.connect()
  await EntryModel.deleteMany()
  await EntryModel.insertMany( seedData.entries )
  await OpenBoardDB.disconnect()

  res.status(200).json({ msg: 'Base de datos reiniciada de fabrica' })

}