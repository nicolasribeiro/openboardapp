import { EntriesProvider } from "@/context/entries";
import { UIProvider } from "@/context/ui";
import "@/styles/globals.css";
import { darkTheme } from "@/themes";
import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider } from "@mui/material/styles";
import type { AppProps } from "next/app";
import { SnackbarProvider } from "notistack";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={darkTheme}>
      <UIProvider>
        <EntriesProvider>
          <SnackbarProvider maxSnack={3}>
            <CssBaseline />
            <Component {...pageProps} />
          </SnackbarProvider>
        </EntriesProvider>
      </UIProvider>
    </ThemeProvider>
  );
}
