import { createTheme }  from '@mui/material/styles';
import { grey }  from '@mui/material/colors/';

export const darkTheme = createTheme({
  palette: {
    mode: "dark",
  },

  components: {
    MuiAppBar: {
      defaultProps: {},
      styleOverrides: {
        root: {
          backgroundColor: grey[800]
        }
      }
    }
  }
});