import { EntryStatus } from "@/interfaces";

interface SeedData {
  entries: SeedEntry[]
}

interface SeedEntry {
  description: string;
  createdAt: number;
  status: EntryStatus
}


export const seedData: SeedData = {
  entries: [
    {
      description: "Pending: Hola Mundo",
      createdAt: Date.now(),
      status: "pending",
    },
    {
      description: "InProgress: Hola Mundo",
      createdAt: Date.now(),
      status: "in-progress",
    },
    {
      description: "Finished: Hola Mundo",
      createdAt: Date.now(),
      status: "finished",
    },
  ]
}