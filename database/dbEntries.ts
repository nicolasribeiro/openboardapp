import { isValidObjectId } from "mongoose"
import { OpenBoardDB } from "."
import { EntryModel, IEntry } from "@/models"


export const getEntryById = async (id: string): Promise<IEntry | null> => {
  if(!isValidObjectId(id)) return null

  await OpenBoardDB.connect()
  const entry = await EntryModel.findById(id).lean()
  await OpenBoardDB.disconnect()

  return JSON.parse(JSON.stringify(entry))
}