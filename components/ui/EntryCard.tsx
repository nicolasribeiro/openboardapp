import Card from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import CardActions from "@mui/material/CardActions";
import React, { DragEvent, useContext } from "react";
import { Entry } from "@/interfaces";
import { UIContext } from "@/context/ui";
import { useRouter } from "next/router";
import { dateFunctions } from "@/utils";
import { capitalize } from "@mui/material/utils";

interface EntryCardProps {
  entry: Entry;
}

export const EntryCard = ({ entry }: EntryCardProps) => {
  const { isDraggin, setIsDraggin } = useContext(UIContext);
  const router = useRouter();

  const onDragStart = (e: DragEvent<HTMLDivElement>) => {
    e.dataTransfer.setData("text", entry._id);
    setIsDraggin(true);
  };
  const onDragEnd = () => {
    setIsDraggin(false);
  };

  const onCardClick = () => {
    router.replace(`/entries/${entry._id}`);
  };

  return (
    <Card
      sx={{
        marginBottom: 1,
        // Evento de drag
      }}
      draggable
      onDragStart={onDragStart}
      onDragEnd={onDragEnd}
      onClick={onCardClick}
    >
      <CardActionArea>
        <CardContent>
          <Typography sx={{ whiteSpace: "pre-line" }}>
            {entry.description}
          </Typography>
        </CardContent>
        <CardActions
          sx={{ display: "flex", justifyContent: "end", padding: 2 }}
        >
          <Typography variant="body2">
            {capitalize(dateFunctions.getFormatDistanceToNow(entry.createdAt))}
          </Typography>
        </CardActions>
      </CardActionArea>
    </Card>
  );
};
