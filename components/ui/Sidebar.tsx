import List from "@mui/material/List";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import ListItem from "@mui/material/ListItem";
import Typography from "@mui/material/Typography";
import InboxOutlinedIcon from "@mui/icons-material/InboxOutlined";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Divider from "@mui/material/Divider";
import { useContext } from "react";
import { UIContext } from "@/context/ui";

const menuItems: string[] = ["Inbox", "Starred", "Send Email"];

export const Sidebar = () => {
  const { isMenuOpen, setIsMenuOpen } = useContext(UIContext);

  return (
    <Drawer
      anchor="left"
      open={isMenuOpen}
      onClose={() => setIsMenuOpen(false)}
    >
      <Box sx={{ width: 250 }}>
        <Box sx={{ padding: "10px" }}>
          <Typography>Menu</Typography>
          <List>
            {menuItems.map((text: string, index: number) => (
              <ListItem key={text}>
                <ListItemIcon>
                  {index % 2 === 0 ? (
                    <InboxOutlinedIcon />
                  ) : (
                    <EmailOutlinedIcon />
                  )}
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
          <Divider />
        </Box>
      </Box>
    </Drawer>
  );
};
