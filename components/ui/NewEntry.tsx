import Button from "@mui/material/Button";
import SaveOutlinedIcon from "@mui/icons-material/SaveOutlined";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import AddCircleOutlineOutlinedIcon from "@mui/icons-material/AddCircleOutlineOutlined";
import { ChangeEvent, useContext, useState } from "react";
import { EntriesContext } from "@/context/entries";
import { v4 as uuidv4 } from "uuid";

export const NewEntry = () => {
  const [inputValue, setInputValue] = useState<string>("");
  const [touched, setTouched] = useState<boolean>(false);
  const { entries, setEntries, setIsFormOpen, isFormOpen, addNewEntry } =
    useContext(EntriesContext);

  const handleInputChange = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setInputValue(e.target.value);
  };

  const onSave = () => {
    if (inputValue.length === 0) return;
    setEntries([
      ...entries,
      {
        _id: uuidv4(),
        description: inputValue,
        createdAt: Date.now(),
        status: "pending",
      },
    ]);
    addNewEntry(inputValue);
    setInputValue("");
    setTouched(false);
  };

  return (
    <Box sx={{ marginBottom: 2, paddingX: 1 }}>
      {isFormOpen ? (
        <>
          <TextField
            fullWidth
            autoFocus
            multiline
            label="Nueva Entrada"
            helperText={
              inputValue.length <= 0 && touched && "Ingresa una tarea"
            }
            error={inputValue.length <= 0 && touched}
            sx={{ marginBottom: 1 }}
            value={inputValue}
            onChange={handleInputChange}
            onBlur={() => setTouched(true)}
          />
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Button
              variant="outlined"
              color="error"
              onClick={() => {
                setTouched(false);
                setIsFormOpen(false);
              }}
            >
              Cancelar
            </Button>
            <Button
              variant="contained"
              color="primary"
              endIcon={<SaveOutlinedIcon />}
              onClick={() => {
                onSave();
                setIsFormOpen(false);
              }}
            >
              Guardar
            </Button>
          </Box>
        </>
      ) : (
        <Button
          startIcon={<AddCircleOutlineOutlinedIcon />}
          fullWidth
          variant="outlined"
          onClick={() => setIsFormOpen(true)}
        >
          Agregar Tarea
        </Button>
      )}
    </Box>
  );
};
