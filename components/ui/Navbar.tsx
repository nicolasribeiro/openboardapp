import AppBar from "@mui/material/AppBar";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import MenuOutlinedIcon from "@mui/icons-material/MenuOutlined";
import { useContext } from "react";
import { UIContext } from "@/context/ui";
import NextLink from "next/link";
import { useTheme } from "@mui/material";

export const Navbar = () => {
  const { setIsMenuOpen } = useContext(UIContext);
  const theme = useTheme();

  return (
    <AppBar position="sticky" elevation={0}>
      <Toolbar>
        <IconButton
          size="large"
          edge="start"
          onClick={() => setIsMenuOpen(true)}
        >
          <MenuOutlinedIcon />
        </IconButton>
        <NextLink
          href="/"
          style={{ textDecoration: "none", color: theme.palette.text.primary }}
        >
          <Typography variant="h6">OpenBoard Next</Typography>
        </NextLink>
      </Toolbar>
    </AppBar>
  );
};
