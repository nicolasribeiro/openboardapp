import Box from "@mui/material/Box";
import Head from "next/head";
import { Navbar, Sidebar } from "../ui";

interface MainLayoutProps {
  title?: string;
  children?: React.ReactNode | undefined;
}

export const MainLayout = ({ title, children }: MainLayoutProps) => {
  return (
    <Box sx={{ flexFlow: 1 }}>
      <Head>
        <title>{title ?? "OpenBoard - NR"}</title>
      </Head>

      <Navbar />
      <Sidebar />
      <Box sx={{ padding: "10px 20px" }}>{children}</Box>
    </Box>
  );
};
