# NextJS OpenBoard by Nicolas Ribeiro

Para correr localmente, se necesita la base de datos

```
docker-compose up -d
```

- El -d, significa **detached**

\*MongoDB URL Local

```
mongodb://localhost:27017/entriesdb
```

## Configurar las variables de entorno

Renombrar el archivo **.env.example** a **.env**

## Llenar la base de datos con informacion de prueba

Llamar a

```
http://localhost:3000/api/seed
```
